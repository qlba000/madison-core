#define _CRT_SECURE_NO_WARNINGS

#include <cuda_runtime_api.h>
#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include "kernel.h"
#include "helpers.h"

// utils
void handleCudaError(cudaError_t error, const char *operation);

// static vars
void
	*hostBuffer,
//	*hostHeap, // heap is not copied to host
	*hostOuts,
	*hostCode,
	*deviceBuffer,
	*deviceHeap,
	*deviceOuts,
	*deviceCode;
int heavyOpsPerBlock,
	outsSizePerBlock,
	codeSizePerBlock,
	maxBlocks,
	maxN;

cudaDeviceProp props;

void ctor()
{
	// set device
	int device = readInt("device number");

	fprintf(stderr, "using device #%d\n", device);

	handleCudaError(cudaSetDevice(device), "setting device");

	// get device properties
	handleCudaError(cudaGetDeviceProperties(&props, device), "getting device props");

	writeLongLong((long long unsigned int)props.sharedMemPerBlock / 4, "Nmax");
	writeLongLong((long long unsigned int)props.totalGlobalMem, "global memory size");
	writeLongLong((long long unsigned int)props.multiProcessorCount, "SMP count");

	fprintf(stderr, "Nmax: %llu floats\n", (long long unsigned int)props.sharedMemPerBlock / 4);
	fprintf(stderr, "Gmax: %llu Gb\n", (long long unsigned int)props.totalGlobalMem / (1 << 30));
	fprintf(stderr, "SMPs: %d\n", props.multiProcessorCount);

	// read parameters
	heavyOpsPerBlock = readInt("heavy operations count per block");
	outsSizePerBlock = readInt("output variables size per block");
	codeSizePerBlock = readInt("code size per block");
	maxBlocks = readInt("max blocks count");
	maxN = readInt("max N");

	fprintf(stderr, "heavyOpsPerBlock: %d\n", heavyOpsPerBlock);
	fprintf(stderr, "outsSizePerBlock: %d\n", outsSizePerBlock);
	fprintf(stderr, "codeSizePerBlock: %d\n", codeSizePerBlock);
	fprintf(stderr, "maxBlocks: %d\n", maxBlocks);
	fprintf(stderr, "maxN: %d\n", maxN);

	if (maxN > props.sharedMemPerBlock / 4)
	{
		fprintf(stderr, "N = %d > Nmax\n", maxN);
		exit(-1);
	}

	// allocate device memory
	int deviceSize = maxBlocks * (heavyOpsPerBlock * maxN * 4 + outsSizePerBlock + codeSizePerBlock);

	fprintf(stderr, "device allocation: %f Gb\n", (float)deviceSize / (1 << 30));

	if (deviceSize > props.totalGlobalMem)
	{
		fprintf(stderr, "device size > Gmax\n");
		exit(-1);
	}

	handleCudaError(cudaMalloc(&deviceBuffer, deviceSize), "allocating device memory");

	// allocate host memory
	int hostSize = maxBlocks * max(outsSizePerBlock, codeSizePerBlock); // heap is not copied to host

	handleCudaError(cudaMallocHost(&hostBuffer, hostSize), "allocating host memory");
}

void dtor()
{
	// dispose allocated memory
	handleCudaError(cudaFreeHost(hostBuffer), "disposing host buffer");
	handleCudaError(cudaFree(deviceBuffer), "disposing device buffer");

	fprintf(stderr, "shutting down...\n");
}

int loop()
{
	double roundStart = omp_get_wtime();

	fprintf(stderr, "---- ---- NEW ROUND ---- ----\n");

	int blocks = readInt("blocks count");

	fprintf(stderr, "blocks: %d\n", blocks);

	if (blocks < 1)
		return -1;

	if (blocks > maxBlocks)
	{
		fprintf(stderr, "blocks = %d > maxBlocks = %d (initialization)\n", blocks, maxBlocks);
		exit(-1);
	}

	int N = readInt("N");

	fprintf(stderr, "N: %d\n", N);

	if (N > maxN)
	{
		fprintf(stderr, "N = %d > maxN = %d (initialization)\n", N, maxN);
		exit(-1);
	}

	// read code
	readStdin(hostBuffer, blocks * codeSizePerBlock, "reading code");

	// load code
	void *deviceCode = lea(deviceBuffer, blocks * (heavyOpsPerBlock * N * 4 + outsSizePerBlock));

	handleCudaError(cudaMemcpy(
		deviceCode, hostBuffer, blocks * codeSizePerBlock, cudaMemcpyHostToDevice), "loading code to device");

	// determine launch params
	int threads = min(props.maxThreadsDim[0], N);

	// ignite kernel
	double kernelStart = omp_get_wtime();

	kernel(
		blocks,
		threads,
		N * 4,
		deviceBuffer,
		heavyOpsPerBlock * N * 4,
		outsSizePerBlock,
		codeSizePerBlock,
		N
	);

	handleCudaError(cudaGetLastError(), "launching kernel");

	// synchronize to device
	handleCudaError(cudaDeviceSynchronize(), "synchronizing to device");

	double kernelEnd = omp_get_wtime();

	// unload results
	void *deviceOuts = lea(deviceBuffer, blocks * heavyOpsPerBlock * N * 4);

	handleCudaError(cudaMemcpy(
		hostBuffer, deviceOuts, blocks * outsSizePerBlock, cudaMemcpyDeviceToHost
	), "retrieving results from device");

	// report results
	writeStdout(hostBuffer, blocks * outsSizePerBlock, "output values");

	double roundEnd = omp_get_wtime();

	double kbt = kernelEnd - kernelStart;

	fprintf(stderr, "burntime: %d.%03d %03d %03d s\n",
		(int)kbt,
		(int)(1e3 * kbt) % 1000,
		(int)(1e6 * kbt) % 1000,
		(int)(1e9 * kbt) % 1000
	);

	fprintf(stderr, "overhead: %.2f %%\n", 100 * (1 - (kernelEnd - kernelStart) / (roundEnd - roundStart)));

	return 0;
}

void handleCudaError(cudaError_t error, const char *operation)
{
	if (error)
	{
		fprintf(stderr, "CUDA error while %s: %s\n", operation, cudaGetErrorString(error));
		exit(error);
	}
}
