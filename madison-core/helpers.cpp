#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "helpers.h"

#ifdef _WIN32
#include <fcntl.h>
#include <io.h>
#endif

void reos()
{
#ifdef _WIN32
	_setmode(_fileno(stdout), O_BINARY);
	_setmode(_fileno(stdin), O_BINARY);
#endif
}

void readStdin(void *dst, int size, const char *subject)
{
	size_t readCount;

	if ((readCount = fread(dst, 1, size, stdin)) != size)
	{
		fprintf(stderr, "FREAD error while reading %s: read %llu bytes, exprected %d, errno: %s\n",
			subject, (long long unsigned int)readCount, size, strerror(errno));
		exit(-1);
	}
}

int readInt(const char *subject)
{
	int res;

	readStdin(&res, 4, subject);

	return res;
}

void writeStdout(void *src, int size, const char *subject)
{
	size_t writeCount;

	if ((writeCount = fwrite(src, 1, size, stdout)) != size)
	{
		fprintf(stderr, "FWRITE error while writing %s: written %llu bytes, exprected %d, errno: %s\n",
			subject, (long long unsigned int)writeCount, size, strerror(errno));
		exit(-1);
	}

	fflush(stdout);
}

void writeLongLong(long long unsigned int data, const char *subject)
{
	writeStdout(&data, 8, subject);
}

void *lea(void *base, int offset)
{
	return (void *)((char *)base + offset);
}

int max(int a, int b)
{
	return a > b ? a : b;
}

int min(int a, int b)
{
	return a < b ? a : b;
}
