#ifndef HELPERS
#define HELPERS

void reos();

void readStdin(void *dst, int size, const char *subject);
int readInt(const char *subject);

void writeStdout(void *src, int size, const char *subject);
void writeLongLong(long long unsigned int data, const char *subject);

void *lea(void *base, int offset);

int max(int a, int b);
int min(int a, int b);

#endif
