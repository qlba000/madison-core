#ifndef INLINES
#define INLINES

#include <vector>
#include "types.h"

#ifdef __CUDACC__
#define CUDABLE __host__ __device__
#else
#define CUDABLE
#endif 

const int MANTISSA = 0b00000000011111111111111111111111;
const int FLOATINF = 0b01111111100000000000000000000000;

CUDABLE inline float setNanValue(int data)
{
	int result = ((data / MANTISSA) << 31) | FLOATINF | (data % MANTISSA + 1);

	return *(float *)&result;
}

CUDABLE inline int getNanValue(float &f)
{
	return ((unsigned int &)(f) >> 31) * MANTISSA + ((unsigned int &)(f) & MANTISSA) - 1;
}

#endif
