#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <device_launch_parameters.h>
#include <cuda_runtime_api.h>
#include "inlines.h"
#include "kernel.h"

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

#define clamp(x) ((x) <= 0 ? 0 : (x) >= 1 ? 1 : (x))

#define TRUTH_EPSILON 1e-4

#define XC_INVALID_METHOD_ASSET 0
#define XC_CRISP 1

__host__ __device__ inline int isxc(float f, int x)
{
	return f != f && getNanValue(f) == x ? 1 : 0;
}

extern __shared__ byte shared[];


__host__ __device__ float T(int type, float a, float b)
{
	switch (type)
	{
	case NORM_MAXMIN:
		return min(a, b);
	case NORM_PROBABILISTIC:
		return a * b;
	case NORM_LUKASIEWICZ:
		return max(a + b - 1, 0);
	case NORM_STRONG:
		return max(a, b) == 1 ? min(a, b) : 0;
	case NORM_NULLPOTENT:
		return a + b > 1 ? min(a, b) : 0;
	default:
		return (float)NAN;
	}
}

__host__ __device__ float S(int type, float a, float b)
{
	switch (type)
	{
	case NORM_MAXMIN:
		return max(a, b);
	case NORM_PROBABILISTIC:
		return a + b - a * b;
	case NORM_LUKASIEWICZ:
		return min(a + b, 1);
	case NORM_STRONG:
		return min(a, b) == 0 ? max(a, b) : 1;
	case NORM_NULLPOTENT:
		return a + b < 1 ? max(a, b) : 1;
	default:
		return (float)NAN;
	}
}

__host__ __device__ float I(int type, float a, float b)
{
	switch (type)
	{
	case IMPLICATION_STANDARD:
		return max(1 - a, b);
	case IMPLICATION_ARITHMETICAL:
		return min(a + b, 1);
	case IMPLICATION_LUKASIEWICZ:
		return min(1, 1 - a + b);
	case IMPLICATION_ALIEV1: /// +
		return a < b ? 1 - a : a > b ? b : 1;
	case IMPLICATION_ALIEV2: /// +
		return a <= b ? 1 : min(1 - a, b);
	case IMPLICATION_ALIEV4: /// +
		return a <= b ? 1 : b / (1 + a - b);
	case IMPLICATION_GODEL: /// +
		return a <= b ? 1 : b;
	case IMPLICATION_RICHEBACH:
		return 1 - a + a * b;
	case IMPLICATION_ZADEH:
		return max(min(a, b), 1 - a);
	case IMPLICATION_RECHER: /// +
		return a <= b ? (float)1 : (float)0;
	case IMPLICATION_CLINCIDENIS:
		return max(1 - a, b);
	case IMPLICATION_HOGUEN: /// +
		return a == 0 ? 1 : min(1, b / a);
	case IMPLICATION_FOUETT:
		return a <= b ? 1 : max(1 - a, b);
	case IMPLICATION_VADHI:
		return max(a * b, 1 - a);
	default:
		return (float)NAN;
	}
}

__host__ __device__ float Al(int type, float y, float w)
{
	float t;

	switch (type)
	{
	case AMPLIFICATION_POWER:
		t = powf(y, w);
		break;
	case AMPLIFICATION_PRESS:
		t = y * w + (1 - w);
		break;
	case AMPLIFICATION_SHIFT:
		t = y + (1 - w);
		break;
	case AMPLIFICATION_DROWN:
		t = max(y, 1 - w);
		break;
	default:
		return (float)NAN;
	}

	return clamp(t);
}

__host__ __device__ float Am(int type, float y, float w)
{
	float t;

	switch (type)
	{
	case AMPLIFICATION_POWER:
		t = powf(y, 1 / w);
		break;
	case AMPLIFICATION_PRESS:
		t = y * w;
		break;
	case AMPLIFICATION_SHIFT:
		t = y - 1 + w;
		break;
	case AMPLIFICATION_DROWN:
		t = min(y, w);
		break;
	default:
		return (float)NAN;
	}

	return clamp(t);
}

__host__ __device__ int getMFSize(byte *def)
{
	int type = ((int *)def)[0];

	switch (type)
	{
	case MEMBERSHIP_DELTA:
		return 2 * 4;
	case MEMBERSHIP_GAUSS:
		return 3 * 4;
	default:
		return INT_MIN;
	}
}


__host__ __device__ float rangeMaximum(float a, float b, byte *factMF)
{
	switch (((int *)factMF)[0])
	{
	case MEMBERSHIP_DELTA:
	{
		float x = ((float *)factMF)[1];

		return a <= x && x <= b ? 1.0f : 0.0f;
	}
	case MEMBERSHIP_GAUSS:
	{
		float m = ((float *)factMF)[1], s = ((float *)factMF)[2];

		if ((m - a) * (m - b) <= 0)
			return 1;
		else if (fabs(m - a) < fabs(m - b))
			return expf(-(m - a) * (m - a) / (s * s));
		else
			return expf(-(m - b) * (m - b) / (s * s));
	}
	default:
		return (float)NAN;
	}
}

__host__ __device__ float rangePeriods(float lo, float hi, byte *premiseMF, byte *factMF)
{
	switch (((int *)premiseMF)[0])
	{
	case MEMBERSHIP_DELTA:
	{
		float x = ((float *)premiseMF)[1];

		if (lo == 0)
		{
			float max1 = rangeMaximum(-INFINITY, nextafterf(x, x - 1), factMF);
			float max2 = rangeMaximum(nextafterf(x, x + 1), +INFINITY, factMF);

			return max(max1, max2);
		}
		else if (hi == 1)
		{
			return rangeMaximum(x, x, factMF);
		}
		else
			return 0;
	}
	case MEMBERSHIP_GAUSS:
	{
		float m = ((float *)premiseMF)[1], s = ((float *)premiseMF)[2];

		float slntaulo = s * sqrtf(-logf(lo));
		float slntauhi = s * sqrtf(-logf(hi));

		float max1 = rangeMaximum(m - slntaulo, m - slntauhi, factMF);
		float max2 = rangeMaximum(m + slntauhi, m + slntaulo, factMF);

		return max(max1, max2);
	}
	default:
		return (float)NAN;
	}
}

__device__ void transformation(float *dst, int t, byte *premiseMF, byte *factMF)
{
	int x = threadIdx.x, h = blockDim.x, *nonzero = (int *)shared;
	float *lastNonzero = (float *)shared + 1;

	if (x == 0)
	{
		*nonzero = 0;
		lastNonzero[0] = 0.f;
		lastNonzero[1] = 0.f;
	}

	__syncthreads();

	for (int i = x; i < t; i += h)
	{
		float lo = (i - 0.5f) / (t - 1);
		float hi = (i + 0.5f) / (t - 1);

		if (lo < 0)
			lo = 0;

		if (hi > 1)
			hi = 1;

		if ((dst[i] = rangePeriods(lo, hi, premiseMF, factMF)) > TRUTH_EPSILON)
		{
			atomicAdd(nonzero, 1);
			lastNonzero[0] = (float)i / (t - 1);
			lastNonzero[1] = dst[i];
		}
	}

	__syncthreads();

	if (x == 0)
		if (*nonzero < 2)
		{
			dst[0] = setNanValue(XC_CRISP);
			dst[1] = lastNonzero[0];
			dst[2] = lastNonzero[1];
		}

	__syncthreads();
}


__device__ void aggregation(float *dst, float *A, float *B, int t, int type, int indexNorm, int valueNorm)
{
	int x = threadIdx.x, h = blockDim.x;
	int *buf = (int *)shared;

	for (int i = x; i < t; i += h)
		buf[i] = 0;

	__syncthreads();

	switch (type)
	{
	case AGGREGATION_AND:
		if (isxc(A[0], XC_CRISP) && isxc(B[0], XC_CRISP))
		{
			if (x == 0)
			{
				dst[0] = setNanValue(XC_CRISP);
				dst[1] = T(indexNorm, A[1], B[1]);
				dst[2] = T(valueNorm, A[2], B[2]);
			}
		}
		else if (isxc(A[0], XC_CRISP))
		{
			if (A[1] == 0)
			{
				if (x == 0)
				{
					dst[0] = setNanValue(XC_CRISP);
					dst[1] = 0;
					dst[2] = 1;
				}
			}
			else
				for (int j = x; j < t; j += h)
				{
					int index = (int)roundf((t - 1) * T(indexNorm, A[1], (float)j / (t - 1)));
					int value = (int)(T(valueNorm, A[2], B[j]) * INT_MAX);

					atomicMax(buf + index, value);
				}
		}
		else if (isxc(B[0], XC_CRISP))
		{
			if (B[1] == 0)
			{
				if (x == 0)
				{
					dst[0] = setNanValue(XC_CRISP);
					dst[1] = 0;
					dst[2] = 1;
				}
			}
			else
				for (int i = x; i < t; i += h)
				{
					int index = (int)roundf((t - 1) * T(indexNorm, (float)i / (t - 1), B[1]));
					int value = (int)(T(valueNorm, A[i], B[2]) * INT_MAX);

					atomicMax(buf + index, value);
				}
		}
		else
		{
			for (int i = x; i < t; i += h)
				for (int j = 0; j < t; j++)
				{
					int index = (int)roundf((t - 1) * T(indexNorm, (float)i / (t - 1), (float)j / (t - 1)));
					int value = (int)(T(valueNorm, A[i], B[j]) * INT_MAX);

					atomicMax(buf + index, value);
				}
		}

		break;
	case AGGREGATION_OR:
		if (isxc(A[0], XC_CRISP) && isxc(B[0], XC_CRISP))
		{
			dst[0] = setNanValue(XC_CRISP);
			dst[1] = S(indexNorm, A[1], B[1]);
			dst[2] = T(valueNorm, A[2], B[2]);
		}
		else if (isxc(A[0], XC_CRISP))
		{
			if (A[1] == 1)
			{
				if (x == 0)
				{
					dst[0] = setNanValue(XC_CRISP);
					dst[1] = 1;
					dst[2] = 1;
				}
			}
			else
				for (int j = x; j < t; j += h)
				{
					int index = (int)roundf((t - 1) * S(indexNorm, A[1], (float)j / (t - 1)));
					int value = (int)(T(valueNorm, A[2], B[j]) * INT_MAX);

					atomicMax(buf + index, value);
				}
		}
		else if (isxc(B[0], XC_CRISP))
		{
			if (B[1] == 1)
			{
				if (x == 0)
				{
					dst[0] = setNanValue(XC_CRISP);
					dst[1] = 1;
					dst[2] = 1;
				}
			}
			else
				for (int i = x; i < t; i += h)
				{
					int index = (int)roundf((t - 1) * S(indexNorm, (float)i / (t - 1), B[1]));
					int value = (int)(T(valueNorm, A[i], B[2]) * INT_MAX);

					atomicMax(buf + index, value);
				}
		}
		else
		{
			for (int i = x; i < t; i += h)
				for (int j = 0; j < t; j++)
				{
					int index = (int)roundf((t - 1) * S(indexNorm, (float)i / (t - 1), (float)j / (t - 1)));
					int value = (int)(T(valueNorm, A[i], B[j]) * INT_MAX);

					atomicMax(buf + index, value);
				}
		}

		break;
	default:
		for (int i = x; i < t; i += h)
			buf[i] = NAN;
		break;
	}

	__syncthreads();

	if (!isxc(dst[0], XC_CRISP))
		for (int i = x; i < t; i += h)
			dst[i] = (float)buf[i] / INT_MAX;

	__syncthreads();

	// Check if result is crisp //

	if (!isxc(dst[0], XC_CRISP))
	{
		int *nonzero = (int *)shared;
		float *lastNonzero = (float *)shared + 1;

		if (x == 0)
		{
			*nonzero = 0;
			lastNonzero[0] = 0.f;
			lastNonzero[1] = 0.f;
		}

		__syncthreads();

		for (int i = x; i < t; i += h)
			if (dst[i] > TRUTH_EPSILON)
			{
				atomicAdd(nonzero, 1);
				lastNonzero[0] = (float)i / (t - 1);
				lastNonzero[1] = dst[i];
			}

		__syncthreads();

		if (x == 0)
			if (*nonzero < 2)
			{
				dst[0] = setNanValue(XC_CRISP);
				dst[1] = lastNonzero[0];
				dst[2] = lastNonzero[1];
			}
	}

	__syncthreads();
}

__device__ void negation(float *dst, float *A, int t)
{
	int x = threadIdx.x, h = blockDim.x, tt = t - 1;

	if (!isxc(A[0], XC_CRISP))
		for (int i = x; i < t; i += h)
			dst[i] = A[tt - i];
	else
		if (x == 0)
		{
			dst[0] = A[0];
			dst[1] = 1.f - A[1];
			dst[2] = A[2];

		}

	__syncthreads();
}


__device__ void activation(int model, float *dst, float *A, float *B, int t, int y, int norm, int implication)
{
	int x = threadIdx.x, h = blockDim.x;

	switch (model)
	{
	case MODEL_LOGICAL:
		for (int j = x; j < y; j += h)
			if (isxc(A[0], XC_CRISP))
			{
				dst[j] = T(norm, A[2], I(implication, A[1], B[j]));
			}
			else
			{
				float maxValue = 0, bj = B[j];

				for (int i = 0; i < t; i++)
				{
					float cv = (float)i / (t - 1);
					//float cv = i == 0 ? 0.5f / (t - 1) : i == t - 1 ? 1.0f - 0.5f / (t - 1) : (float)i / (t - 1);

					float value = T(norm, A[i], I(implication, cv, bj));

					if (maxValue < value)
						maxValue = value;
				}

				dst[j] = maxValue;
			}

		break;
	case MODEL_MAMDANI:
		for (int j = x; j < y; j += h)
			if (isxc(A[0], XC_CRISP))
			{
				dst[j] = T(norm, A[2], T(implication, A[1], B[j]));
			}
			else
			{
				float maxValue = 0, bj = B[j];

				for (int i = 0; i < t; i++)
				{
					float cv = (float)i / (t - 1);
					//float cv = i == 0 ? 0.5f / (t - 1) : i == t - 1 ? 1.0f - 0.5f / (t - 1) : (float)i / (t - 1);

					float value = T(norm, A[i], T(implication, cv, bj));

					if (maxValue < value)
						maxValue = value;
				}

				dst[j] = maxValue;
			}

		break;
	default:
		dst[0] = (float)NAN;
	}

	__syncthreads();
}


__device__ void amplification(int model, float *dst, float *A, int y, float weight, int method)
{
	int x = threadIdx.x, h = blockDim.x;

	switch (model)
	{
	case MODEL_LOGICAL:
		for (int j = x; j < y; j += h)
			dst[j] = Al(method, A[j], weight);
		break;
	case MODEL_MAMDANI:
		for (int j = x; j < y; j += h)
			dst[j] = Am(method, A[j], weight);
		break;
	default:
		dst[0] = (float)NAN;
	}
}


__device__ void accumulation(int model, float *dst, float *A, float *B, int y)
{
	int x = threadIdx.x, h = blockDim.x;

	switch (model)
	{
	case MODEL_LOGICAL:
		for (int i = x; i < y; i += h)
			dst[i] = min(A[i], B[i]);
		break;
	case MODEL_MAMDANI:
		for (int i = x; i < y; i += h)
			dst[i] = max(A[i], B[i]);
		break;
	default:
		dst[0] = (float)NAN;
	}

}


__device__ void defuzzification(float *dst, float *A, int y, int method, float a, float b)
{
	int x = threadIdx.x, h = blockDim.x;

	switch (method)
	{
	case DEFUZZIFICATION_COG:
	{
		float *grvSumTotal = (float *)shared;
		float *momSumTotal = grvSumTotal + 1;
		float grvSum = 0, momSum = 0;

		if (x == 0)
		{
			*grvSumTotal = 0;
			*momSumTotal = 0;
		}

		__syncthreads();

		for (int i = x; i < y; i += h)
		{
			float shoulder = a + (b - a) * i / (y - 1);
			float gravity = A[i];

			momSum += gravity * shoulder;
			grvSum += gravity;
		}

		atomicAdd(grvSumTotal, grvSum);
		atomicAdd(momSumTotal, momSum);

		__syncthreads();

		if (x == 0)
		{
			float result = *momSumTotal / *grvSumTotal;

			if (isnan(result))
				*dst = (a + b) / 2;
			else
				*dst = result;
		}

		break;
	}
	case DEFUZZIFICATION_COA:
	{
		int x = threadIdx.x, h = blockDim.x;

		float *areaSumTotal = (float *)shared;
		float areaSum = 0;

		if (x == 0)
			*areaSumTotal = 0;

		__syncthreads();

		for (int i = x; i < y; i += h)
			areaSum += A[i];

		atomicAdd(areaSumTotal, areaSum);

		__syncthreads();

		if (x == 0)
		{
			if (!(*areaSumTotal > 0))
				*dst = (a + b) / 2;
			else
			{
				areaSum = *areaSumTotal / 2;

				for (int i = 0; i < y; i++)
				{
					areaSum -= A[i];

					if (areaSum <= 0)
					{
						*dst = a + (b - a) * i / (y - 1);
						break;
					}
				}
			}
		}

		break;
	}
	default:
		*dst = NAN;
		return;
	}
}



__device__ void tabulate(float *dst, int n, float a, float b, byte *MF)
{
	int x = threadIdx.x, h = blockDim.x;

	for (int i = x; i < n; i += h)
	{
		float lo = a + (b - a) * (i - 0.5f) / (n - 1);
		float hi = a + (b - a) * (i + 0.5f) / (n - 1);

		if (lo < a)
			lo = a;

		if (hi > b)
			hi = b;

		dst[i] = rangeMaximum(lo, hi, MF);
	}

	__syncthreads();
}

__global__ void entry(byte *base, int heapSize, int outsSize, int codeSize, int n)
{
	int sysIndex = blockIdx.x, sysCount = gridDim.x;

	float *heap = (float *)(base + sysIndex * heapSize);
	float *outs = (float *)(base + sysCount * heapSize + sysIndex * outsSize);
	byte *code = base + sysCount * heapSize + sysCount * outsSize + sysIndex * codeSize;

	byte *ip = code;

	int heapCounter = 0;

	for (;;)
	{
		int instruction = *(int *)ip;

		ip += 4;

		switch (instruction)
		{
		case OP_RETURN:
			return;
		case OP_FTV_TRANSFORMATION:
		{
			int premiseMFSize = getMFSize(ip), factMFSize = getMFSize(ip + premiseMFSize);

			transformation(heap + heapCounter++ * n, n, ip, ip + premiseMFSize);

			ip += premiseMFSize + factMFSize;
			break;
		}
		case OP_FTV_AGGREGATION_CONJUNCTION:
		case OP_FTV_AGGREGATION_DISJUNCTION:
		{
			int A = ((int *)ip)[0];
			int B = ((int *)ip)[1];
			int indexNorm = ((int *)ip)[2];
			int valueNorm = ((int *)ip)[3];

			aggregation(
				heap + heapCounter++ * n,
				heap + A * n,
				heap + B * n,
				n,
				instruction == OP_FTV_AGGREGATION_CONJUNCTION ? AGGREGATION_AND : AGGREGATION_OR,
				indexNorm,
				valueNorm
			);

			ip += 4 * 4;
			break;
		}
		case OP_FTV_NEGATION:
		{
			int A = ((int *)ip)[0];

			negation(heap + heapCounter++ * n, heap + A * n, n);

			ip += 4;
			break;
		}
		case OP_FTV_ACTIVATION:
		{
			int A = ((int *)ip)[0];

			int MFSize = getMFSize(ip + 4);

			float a = ((float *)(ip + MFSize))[1];
			float b = ((float *)(ip + MFSize))[2];
			int model = ((int *)(ip + MFSize))[3];
			int norm = ((int *)(ip + MFSize))[4];
			int implication = ((int *)(ip + MFSize))[5];

			tabulate((float *)shared, n, a, b, ip + 4);

			activation(
				model,
				heap + heapCounter++ * n,
				heap + A * n,
				(float *)shared,
				n,
				n,
				norm,
				implication
			);

			ip += 6 * 4 + MFSize;
			break;
		}
		case OP_OMF_AMPLIFICATION:
		{
			int A = ((int *)ip)[0];
			float weight = ((float *)ip)[1];
			int model = ((int *)ip)[2];
			int method = ((int *)ip)[3];

			amplification(
				model,
				heap + heapCounter++ * n,
				heap + A * n,
				n,
				weight,
				method
			);

			ip += 4 * 4;
			break;
		}
		case OP_OMF_ACCUMULATION:
		{
			int A = ((int *)ip)[0];
			int B = ((int *)ip)[1];
			int model = ((int *)ip)[2];

			accumulation(
				model,
				heap + heapCounter++ * n,
				heap + A * n,
				heap + B * n,
				n
			);

			ip += 3 * 4;
			break;
		}
		case OP_OMF_DEFUZZIFICATION:
		{
			int source = ((int *)ip)[0];
			int method = ((int *)ip)[1];
			float a = ((float *)ip)[2];
			float b = ((float *)ip)[3];

			defuzzification(outs++, heap + source * n, n, method, a, b);

			ip += 4 * 4;
			break;
		}
		default:
			outs[0] = NAN;
			return;
		}
	}
}

void kernel(int blocks, int threads, int sharedSize,
	void *base, int heapSize, int outsSize, int codeSize, int n)
{
	entry<<<blocks, threads, sharedSize>>>((byte *)base, heapSize, outsSize, codeSize, n);
}
