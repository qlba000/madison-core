#ifndef KERNEL
#define KERNEL

#include "kernel_enums.h"
#include "types.h"

void kernel(
	int blocks,
	int threads,
	int sharedSize,
	void *base,
	int heapSize,
	int outsSize,
	int codeSize,
	int n
);

#endif
