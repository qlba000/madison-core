#include <stdlib.h>
#include <stdio.h>
#include "cuda_fuser.h"
#include "helpers.h"

int main(int argc, char **argv)
{
	fprintf(stderr, "MADISON-CORE batch fuzzy inference framework\n");

	reos();
	ctor();

	while (!loop());

	dtor();

	return 0;
}
